# frozen_string_literal: true

# Picks up and executes DDL files under
# - db/migrate/roles
# - db/migrate/functions
# - db/migrate/views
#
# New rake tasks are
# - db:create_roles
# - db:create_functions
# - db:create_views
# - db:drop_roles
# - db:drop_functions
# - db:drop_views
# and the convenience tasks `db:create_additional_objects` and `db:drop_additional_objects`, which do them all.
#
# `db:create_additional_objects` will run automatically after each `db:migrate`
#
# Files must have names like "object_name.sql" or "001_object_name.sql", where
#  "object_name" must be identical to the actual database object name.
# Those files can also be ERBs (need additionally the .erb extension)

Rake::Task['db:migrate'].enhance do
  Rake::Task['db:create_additional_objects'].invoke # as block, so it runs _after_ the task
end
Rake::Task['db:prepare'].enhance do
  Rake::Task['db:create_additional_objects'].invoke # as block, so it runs _after_ the task
end
# Rake::Task['db:structure:dump'].enhance(%w[db:drop_additional_objects])

namespace :db do
  desc 'Drops DB roles/users, functions and views described in db/*/*'
  task drop_additional_objects: %i[drop_views drop_functions drop_roles]

  desc 'Creates each DB role/user, function and view described in db/*/*, unless it already exists'
  task create_additional_objects: %i[create_roles create_functions create_views]

  desc 'Drops all DB roles/users described in db/migrate/roles/*'
  task drop_roles: :environment do
    ddl_files_under('roles').each do |sql_file|
      next unless role_exists?(sql_file)

      role_name = db_object_name(sql_file)
      conn.execute "DROP OWNED BY #{role_name} CASCADE"
      conn.execute "DROP ROLE #{role_name}" # this will fail if role_name owns objects/permissions in other databases
    end
  end

  desc 'Drops all DB functions described in db/migrate/functions/*'
  task drop_functions: :environment do
    ddl_files_under('functions')
      .each { |sql_file| conn.execute "DROP FUNCTION IF EXISTS #{db_object_name(sql_file)} CASCADE" }
  end

  desc 'Drops all DB views described in db/migrate/views/*'
  task drop_views: :environment do
    ddl_files_under('views')
      .each { |sql_file| conn.execute "DROP VIEW IF EXISTS #{db_object_name(sql_file)} CASCADE" }
  end

  desc 'Creates each DB roles/users described in db/migrate/roles/*, unless it already exists'
  task create_roles: :environment do
    ddl_files_under('roles').each do |sql_file|
      role_exists?(sql_file) ? skip_sql_file(sql_file) : execute_sql_file(sql_file)
    end
  end

  desc 'Creates each DB function described in db/migrate/functions/*, unless it already exists'
  task create_functions: :environment do
    ddl_files_under('functions').each do |sql_file|
      function_exists?(sql_file) ? skip_sql_file(sql_file) : execute_sql_file(sql_file)
    end
  end

  desc 'Creates each DB view described in db/migrate/views/*, unless it already exists'
  task create_views: :environment do
    ddl_files_under('views').each do |sql_file|
      view_exists?(sql_file) ? skip_sql_file(sql_file) : execute_sql_file(sql_file)
    end
  end

  private

  # a DB connection for running raw SQL
  def conn
    @conn ||= ActiveRecord::Base.connection
  end

  # returns all DDLs in a subdir in an order that fulfills all build order dependencies
  def ddl_files_under(subdir)
    Dir.glob("#{Rails.root}/db/migrate/#{subdir}/*.sql{,.erb}").sort
  end

  def db_object_name(file_name)
    file_name.split('/').last.match(/^(\d*_)?(.*).sql(.erb)?$/)[2]
  end

  def role_exists?(file_name)
    role_name = db_object_name(file_name)
    db_object_exists? "SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = '#{role_name}'"
  end

  def function_exists?(file_name)
    function_name = db_object_name(file_name)
    db_object_exists? "SELECT 1 FROM information_schema.routines WHERE routine_schema = 'public' AND routine_type = 'FUNCTION' AND routine_name = '#{function_name}'"
  end

  def view_exists?(file_name)
    view_name = db_object_name(file_name)
    db_object_exists? "SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'VIEW' AND table_name = '#{view_name}'"
  end

  def db_object_exists?(sql)
    conn.execute(sql).ntuples.positive?
  end

  def execute_sql_file(file_name)
    puts "Building #{file_name.split('/')[-2]} -> #{db_object_name(file_name)}"
    f_content = File.read(file_name)
    sql = file_name.end_with?('.erb') ? ERB.new(f_content).result : f_content
    conn.execute(sql)
  end

  def skip_sql_file(file_name)
    puts "Skipping #{file_name.split('/')[-2]} -> #{db_object_name(file_name)}"
  end
end
