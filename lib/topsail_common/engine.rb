module TopsailCommon
  class Engine < ::Rails::Engine
    initializer 'topsail-common.importmap', before: 'importmap' do |app|
      app.config.importmap.paths << Engine.root.join('config/importmap.rb')
    end
  end
end
