#
# Just like formbuilder.label(..., &block)
#
module TopsailCommon::FormBuilder::TopLabel
  def top_label(method, options_or_label_content = nil, **options, &block)
    if options_or_label_content.is_a?(Hash)
      options = options_or_label_content
      label_text = nil
    else
      label_text = options_or_label_content
    end
    options[:class] = @template.class_names(options[:class], 'top_labeled')

    @template.tag.div(**options) do
      label(method, label_text) +
        input_help_text_tag(method) +
        @template.tag.div(class: 'top_labeled_content', &block)
    end
  end

  private

  # @return a DOM element with help text, or nil
  def input_help_text_tag(method)
    return unless (help_text = input_help_text(method))

    @template.help_hover(help_text, class: 'help_text')
  end

  # help text or nil;
  # looks in I18n yaml at key
  # en:
  #   help:
  #     the_model_name:
  #       attribute_html:    or    attribute:
  def input_help_text(method)
    # strangely you can't check in Rails 5.x if an object is an ActiveModel or includes ActiveModel::Naming
    return unless (i18key = object.try(:model_name)&.i18n_key)

    @template.t("#{method}_html", scope: [:help, i18key], default: '').presence ||
      @template.t(method, scope: [:help, i18key], default: '').presence
  end
end

ActionView::Helpers::FormBuilder.include TopsailCommon::FormBuilder::TopLabel
