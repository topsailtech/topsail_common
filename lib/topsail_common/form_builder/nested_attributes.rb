module TopsailCommon::FormBuilder::NestedAttributes
  include ActionView::Helpers::JavaScriptHelper

  # @param name[String]               - just like first attribute for "regular" link_to function
  # @param association[Symbol]        - for which attribute fields to created the nested fields
  #
  # Optional Parameters (see function definition for default values):
  # @param partial[String]            - the partial to use for rendering the nested fields form
  # @param form_builder_name[Symbol]  - the variable name of the form builder in the partial
  # @param nested_object[ActiveModel] - the empty object used for building the form;
  #                                     this is useful if the nested form is not for a real AR assiciation, or if you want to set certain defaults
  # @param locals[Hash]               - passed through as locals to the :partial
  # @param container_selector[String] - the JavaScript fragment for finding the container where the new form should be appended.
  #                                     It references the link as "this".
  #                                     E.g. container_selector: 'this.previousElementSibling'
  # @param fields_added[String]       - JavaScript fragment for callback function after fields are added.
  #                                     The only argument for this fn is the array of root nodes of the added DOM fragment;
  #                                     E.g. fields_added: 'function(row){console.log(row)}'
  # all other options are being passed through to link_to as html_options
  def link_to_add_fields(name,
                         association,
                         partial:              "#{association.to_s.singularize}_fields",
                         form_builder_name:    :f,
                         nested_object:        nil,
                         locals:               {},
                         container_selector:   'this.previousElementSibling',
                         fields_added:         nil,
                         **html_options)
    unless nested_object
      nested_object = object.send(association).build
      object.send(association).delete(nested_object) # makes sure we don'ty keep the dummy in the actual assoc
    end
    form_builder_placeholder = "new_#{association}_PLACEHOLDER"
    fields = fields_for(association, nested_object, child_index: form_builder_placeholder) do |builder|
      locals[form_builder_name] = builder
      @template.render(partial, locals)
    end

    js = "var t=\"#{escape_javascript(fields)}\".replace(new RegExp('#{form_builder_placeholder}', 'g'), new Date().getTime()); #{container_selector}.insertAdjacentHTML('beforeend', t);"
    js += "(#{fields_added})(t)" unless fields_added.blank?
    js += ';return false'

    @template.link_to(name, '#', html_options.merge(onclick: js))
  end

  # @param name[String] - just like first attribute for "regular" link_to function)
  #
  # Optional Parameters (see function definition for default values):
  # @param container_selector[String]  - the selector for the container that should be hidden when clicking. Defaults to '$(this).parent()'
  # @param before_fields_remove[Strig] - JavaScript fragment for callback function before removing field;
  #                                      The only argument for this fn is the container dom fragment.
  #                                      Must return true in order to proceed with field removal.
  #                                      E.g. before_fields_remove: 'function(row){return confirm("Are you sure?")}'
  # @param fields_removed[Strig]       - JavaScript fragment for callback function after field is removed;
  #                                      The only argument for this fn is the container dom fragment.
  #                                      E.g. fields_removed: 'function(row){console.log("Removing DOM row", row)}'
  # all other options are being passed through to link_to as html_options
  def link_to_remove_fields(name = 'Remove',
                            container_selector:   'this.parentElement',
                            before_fields_remove: nil,
                            fields_removed:       nil,
                            **html_options)
    js = ''
    js += "if (!(#{before_fields_remove})(#{container_selector})){return false};" unless before_fields_remove.blank?
    js += "this.previousElementSibling.value=1; #{container_selector}.hidden = true;"
    js += "(#{fields_removed})(#{container_selector})" unless fields_removed.blank?
    js += ';return false'

    hidden_field(:_destroy) + @template.link_to(name, '#', html_options.merge(onclick: js))
  end
end

ActionView::Helpers::FormBuilder.send :include, TopsailCommon::FormBuilder::NestedAttributes
