module TopsailCommonHelper
  def body_class
    ctrl_name = controller.class.name.underscore.sub(/_controller$/,'') # like 'module/some_class'
    action_name = params[:action]
    variant_name = request.variant.first
    "controller_#{ctrl_name} action_#{action_name} variant_#{variant_name}"
  end
end