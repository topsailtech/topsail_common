module TopsailCommonTagHelper
  # creates <i class="iconify" data-icon="mdi:food-apple"></i>
  # @param icon_name e.g. 'mdi:apple', or 'mdi:food-apple', or 'emojione-v1:cat-face'
  #                  the iconset defaults to "mdi:" if no such namespace if given
  def iconify(icon_name, **html_opts)
    icon_name = "mdi:#{icon_name}" unless icon_name.include?(':')
    (html_opts[:data] ||= {})[:icon] = icon_name
    html_opts[:class] = class_names(html_opts[:class], 'iconify-inline')
    tag.i(**html_opts)
  end

  # Create a little hoverable help symbol.
  # When hovering, show :text or content of given block.
  #
  # requires custom element <ts-tooltip>
  def help_hover(text = nil, **atts, &block)
    safe_join([
      tag.span(iconify('mdi:help-circle'), **atts),
      if block_given?
        tag.ts_tooltip(&block)
      else
        tag.ts_tooltip(text)
      end
    ])
  end
end
