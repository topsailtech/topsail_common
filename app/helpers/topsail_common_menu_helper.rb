module TopsailCommonMenuHelper
  def menu_links(menu_configs_ary, html_opts = {})
    content_tag(
      :ul,
      safe_join(menu_configs_ary.map{ |item_cfg| menu_link(**item_cfg) }),
      html_opts
    )
  end

  private

  # build an individual menu link (or submenu entry),
  # incl. check for permission using Pundit or explicit
  # @permitted parameter.
  # Simplest case:  menu_link(model: Phone)
  def menu_link(label: nil,
                icon: nil,
                model: nil,
                url: nil,
                permitted: nil,
                submenu: nil,
                html_opts: {}) # extra html options for the menu link or submenu UL tag
    if menu_permitted?(submenu: submenu, permitted: permitted, model: model)
      content_tag :li do
        if submenu.nil?
          link_to(menu_label(label: label, icon: icon, model: model),
                  url || model,
                  html_opts)
        else
          submenu_foldout(label: label,
                          icon: icon,
                          model: model,
                          permitted: permitted,
                          submenu: submenu,
                          html_opts: html_opts)
        end
      end
    end
  end

  def submenu_foldout(label: nil,
                      icon: nil,
                      model: nil,
                      permitted: nil,
                      submenu: nil,
                      html_opts: {}) # extra html options for a submenu's UL tag
    show_submenu = permitted.nil? ? any_menu_permitted?(submenu) : permitted
    return unless show_submenu

    content_tag(:h2, menu_label(label: label, icon: icon, model: model)) +
      menu_links(submenu, html_opts)
  end

  def any_menu_permitted?(menu_configs_ary)
    menu_configs_ary.any? { |a_menu| menu_permitted?(**a_menu.slice(:submenu, :permitted, :model)) }
  end

  def menu_permitted?(submenu: nil, permitted: nil, model: nil)
    if !submenu.nil?
      any_menu_permitted? submenu
    elsif !permitted.nil?
      permitted
    elsif model
      policy(model).show_in_menu?
    else  # some static link, like "Logout" etc
      true
    end
  end

  def menu_label(label: nil, icon: nil, model: nil)
    full_text = "".html_safe
    full_text << iconify(icon) unless icon.nil?
    full_text << h(label || model.model_name.human(count: :many))
  end
end
