module ScopesForDate
  extend ActiveSupport::Concern

  included do
    def self.scopes_for_date(date_column)

      date_column = date_column.to_s

      scope "#{date_column}_on_or_after", lambda { |date_from| # Date or Time
        where [ "#{table_name}.#{date_column} >= ?", date_from ]
      }

      scope "#{date_column}_on_or_before", lambda { |date_to| # Date or Time
        if date_to.is_a? Date
          where [ "#{table_name}.#{date_column} < ?", date_to.next ]
        else
          where [ "#{table_name}.#{date_column} <= ?", date_to ]
        end
      }
      
      scope "#{date_column}_during", ->(*args){ # nil, Date, Date Range or Time Range
        range = if args[0].nil?
                  Date.current.to_time..Date.current.end_of_day
                elsif args[0].is_a? Date
                  args[0].to_time..args[0].end_of_day
                elsif args[0].is_a?(Range) && args[0].end.is_a?(Date)
                  args[0].begin.to_time..args[0].end.end_of_day
                elsif args[0].is_a?(Range)
                  args[0].begin.to_time..args[0].end.to_time
                elsif args[0].is_a?(String) # a single Date String
                  args[0].to_time..args[0].to_date.end_of_day
                else
                  args[0] # not sure what it actually could be here
                end
        where date_column => range
      }
    end
  end
end