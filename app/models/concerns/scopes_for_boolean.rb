module ScopesForBoolean
  extend ActiveSupport::Concern

  included do
    def self.scopes_for_boolean(boolean_column)
      boolean_column = boolean_column.to_s

      # exceptions can be
      # - nil,
      # - array/list of record ids e.g. `active(12,13)` or `active([12, 13])`
      # - ActiveRecord
      # - array/list of ActiveRecords
      scope boolean_column, lambda { | *exceptions |
        where(boolean_column => true).or(where(id: exceptions))
      }

      scope "not_#{boolean_column}", lambda {
        where(boolean_column => false)
      }
    end
  end
end
