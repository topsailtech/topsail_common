import "@iconify/iconify"
import "ts-tooltip"

import { application } from "controllers/application"
import TomSelectController from "topsail_common/controllers/tom_select_controller";
application.register("tom-select", TomSelectController);
