export { backgroundFlash }
// import 'stylesheets/components/effect.scss'

function backgroundFlash(el){
  _runEffect(_visibleElement(el), "topsail_effect_background_flash");
}

// not always is the DOM element that we want to highlight the actually visisble
// element. Trying to find it.
function _visibleElement(el){
  if (el.getAttribute("is") == "ts-select2"){
    return el.nextElementSibling.querySelector(".select2-selection")
  } else {
    return el
  }
}

function _runEffect(el, effect_css_class){
  el.addEventListener("transitionend", function(){
    this.classList.remove("topsail_effect", "topsail_effect_running", effect_css_class)
  }, { once : true });
  el.classList.add("topsail_effect", effect_css_class);
  setTimeout(function(){ el.classList.add("topsail_effect_running"); }, 10);
}
