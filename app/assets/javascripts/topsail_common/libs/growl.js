export { growl, info, warn, error }
// import 'stylesheets/components/growl.scss'

import Toastify from "toastify-js"

// Low level call
function growl(text, opts = {}){
  Toastify({
    text: text,
    duration: opts.duration || 3000, // in ms; -1 for permanent toast
    close: true,
    className: opts.className
  }).showToast();
}

function info(text){
  growl(text, { className: 'info' })
}

function warn(text){
  growl(text, { className: 'warn', duration: -1 })
}

function error(text){
  growl(text, { className: 'error', duration: -1 })
}
