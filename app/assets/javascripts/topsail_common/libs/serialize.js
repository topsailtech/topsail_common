export { serializeElement }

/*
 * Serializes a FORM element.
 *
 * Taken from Rails UJS library
 * https://github.com/rails/rails/blob/7-2-stable/actionview/app/assets/javascripts/rails-ujs.esm.js
 * Released under the MIT license
 */

const m = Element.prototype.matches || Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector;

const matches = function(element, selector) {
  if (selector.exclude) {
    return m.call(element, selector.selector) && !m.call(element, selector.exclude);
  } else {
    return m.call(element, selector);
  }
};

const toArray = e => Array.prototype.slice.call(e);

const serializeElement = (element, additionalParam) => {
  let inputs = [ element ];
  if (matches(element, "form")) {
    inputs = toArray(element.elements);
  }
  const params = [];
  inputs.forEach((function(input) {
    if (!input.name || input.disabled) {
      return;
    }
    if (matches(input, "fieldset[disabled] *")) {
      return;
    }
    if (matches(input, "select")) {
      toArray(input.options).forEach((function(option) {
        if (option.selected) {
          params.push({
            name: input.name,
            value: option.value
          });
        }
      }));
    } else if (input.checked || [ "radio", "checkbox", "submit" ].indexOf(input.type) === -1) {
      params.push({
        name: input.name,
        value: input.value
      });
    }
  }));
  if (additionalParam) {
    params.push(additionalParam);
  }
  return params.map((function(param) {
    if (param.name) {
      return `${encodeURIComponent(param.name)}=${encodeURIComponent(param.value)}`;
    } else {
      return param;
    }
  })).join("&");
};
