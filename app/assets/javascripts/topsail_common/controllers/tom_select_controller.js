/*
  select options: See static values
*/
import { Controller } from "@hotwired/stimulus"
import TomSelect from 'tom-select';
import DragulaPlugin from 'topsail_common/tom_select_plugins/dragula_plugin'

TomSelect.define('dragula', DragulaPlugin);

export default class extends Controller {
  static values = {
    // data-tom-select-ajax-url-value; if present, we'll do Ajax calls to fetch options
    ajaxUrl:            String,

    // data-tom-select-ajax-param-value
    ajaxParam:          { type: String, default: 'filter[q]' },
    ajaxPageParam:      { type: String, default: 'paginate[page]' },
    ajaxMinInputLength: { type: Number, default: 3 },

    // data-tom-select-optgroup-add-all-value; if true, optgroups get an "Add all" link to add all options within
    optgroupAddAll:     Boolean, // default: false

    optionCreatePrompt: String,

    // data-tom-select-orderable-value; for Drag-n-Drop of options
    orderable:           Boolean, // default: false

    // data-tom-select-options-value; other pass-through initialization options for tom-select
    options:            Object
  };

  connect() {
    this.tomselect = new TomSelect(
      this.element,
      Object.assign({}, this._tom_select_opts(), this.optionsValue)
    );
  }

  disconnect() {
    this.tomselect.destroy();
  }

  _tom_select_opts(){
    const opts = {
      maxOptions: null,
      plugins: this._plugins(),
    }
    Object.assign(opts, this._optionsForAjax())
    Object.assign(opts, this._optionsForRendering())
    return opts
  }

  _optionsForAjax() {
    if (this.ajaxy()){
      return {
        firstUrl: (query)=>this._ajaxQueryUrl(query),
        load: (query, callback)=>{
                fetch(this.tomselect.getUrl(query))
                  .then(response=> response.json())
                  .then(json=> {
                    this.tomselect.setNextUrl(
                      query,
                      json.hasMore && `${this._ajaxQueryUrl(query)}&${this.ajaxPageParamValue}=${json.page + 1}`
                    )
                    callback(json.records)
                  })
                  .catch(()=> callback())
              },
        shouldLoad: (query)=> query.length >= this.ajaxMinInputLengthValue,
        score: function(_search){ return function(_item){ return 1 } }  // they always match
      }
    } else {
      return {}
    }
  }

  _ajaxQueryUrl(query){
    const separator = this.ajaxUrlValue.includes("?") ? "&" : "?"
    return `${this.ajaxUrlValue}${separator}${this.ajaxParamValue}=${encodeURIComponent(query)}`
  }

  _optionsForRendering(){
    const callbacks = {
      // use "short_html" or "long_html", with fallback "text".
      // The *_html values will not be html-escaped, but the "text" attribute / DOM.text() will be escaped.
      // We use snake_case instead of camelCase attributes since on non-ajax options the atts are being downcased.
      option:  (data, escape)=>`<div>${data.long_html || escape(data[this.optionsValue.labelField || 'text'])}</div>`,
      item:    (data, escape)=>`<div>${data.short_html || escape(data[this.optionsValue.labelField || 'text'])}</div>`
    }
    if (this.optgroupAddAllValue){
      callbacks.optgroup_header = this._renderOptGroupAddAllHeading.bind(this)
    }
    if (this.hasOptionCreatePromptValue){
      callbacks.option_create = (data, escape)=>`<div class="create">${this.optionCreatePromptValue} <strong>${escape(data.input)}</strong>&hellip;</div>`
    }
    return { render: callbacks }
  }

  _plugins(){
    const plugins = ['dropdown_input']
    if (this.element.multiple) { // multi-select
      plugins.push('remove_button')
     } else if(this.element.options[0]?.value == '') { // single-select with placeholder
      plugins.push('clear_button')
     }
    this.ajaxy() && plugins.push('virtual_scroll')
    this.orderableValue && this.element.multiple && plugins.push('dragula')
    return plugins
  }

  ajaxy(){
    return !!this.ajaxUrlValue
  }

  _renderOptGroupAddAllHeading(data, escape){
    // Build the "add" link
    const add = document.createElement("a")
    add.href = "#"
    add.innerHTML = "add all"
    add.style.float = "right"
    add.addEventListener("click", (evt)=>{
      evt.preventDefault()
      evt.stopPropagation()
      const new_vals = [],
            all_options = this.tomselect.options
      for (let result_item of this.tomselect.currentResults.items) {
        let option = all_options[result_item.id],
            optgrp = option[this.optionsValue.optgroupField] || option.optgroup;
        (optgrp == data.value) && new_vals.push(option.value)
      }
      this.tomselect.addItems(new_vals, true)
    })

    // Build the entire thing
    const heading = document.createElement("div")
    heading.classList.add("optgroup-header")
    heading.innerText = data[this.tomselect.settings.optgroupLabelField] // not escaping. Should already be escaped from OPTGROUP element
    heading.appendChild(add)
    return heading
  }
}
