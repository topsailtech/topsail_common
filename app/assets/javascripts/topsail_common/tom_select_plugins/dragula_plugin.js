import dragula from "dragula"

/*
	Drag and Drop plugin for tom-select.

	Based on the tom-select's
	"drag_drop" plugin (https://github.com/orchidjs/tom-select/blob/master/src/plugins/drag_drop/plugin.ts),
	but uses Dragula (https://github.com/bevacqua/dragula) instead of jQuery and jQueryUI
 */
export default function(plugin_options) {
	// plugin_options: plugin-specific options
	// this: TomSelect instance

	this.on('initialize', ()=>{
		let drake = dragula([this.control], {
			mirrorContainer: this.control.parentElement, // in case control is in a DIALOG
			moves:           (el, _source, _handle, _sibling)=>el.hasAttribute("data-value") && !this.isLocked
		})
		drake.on("drop", (el, target, source, sibling)=>{
			const values = []
			Array.from(target.children).forEach((el)=>{
				if (el.hasAttribute("data-value") && el.dataset.value){
					values.push(el.dataset.value)
				}
			})
			this.setValue(values)
		})
	})
}
