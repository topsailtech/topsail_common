# TopsailCommon

Provides shared, low-level code for topsail projects.

* SCSS variables, functions and styles
* UI Helpers
    * [menu_links](#markdown-header-using-view-helper-menu_linksmenu_configs_ary)
    * body_class
    * help_hover
* form builder extensions
    * nested_fields
    * [top_label](#markdown-header-using-form-builder-helper-top_label)
* Scopes
    * [scopes_for_boolean](#markdown-header-scopes_for_boolean)
* Possibly some JS libs (probably not)
    * GrowableTable
* Preliminary dumping ground
    * GIS
	* Ansible playbooks
	* initializers for topsail project conventions

## Installation
Gemfile:

```ruby
git_source(:topsail_gem){ |repo_name| "https://bitbucket.org/topsailtech/#{repo_name}.git" }
gem 'topsail_common', topsail_gem: 'topsail_common'
```

## Add CSS variables and styles
Add to your asset scss file
```scss
@use "topsail_common";
```
This adds SimpleCSS and some basic topsail styling to your SCSS stylesheet. Most is customizable be specifying your own CSS properties.

## Utilize javascript modules
- declare `stimulus.js` in your application's import-map
- add to your `application.js`
    ```javascript
        import "topsail_common/application"
    ```
### ModalDialog Controller
### SlimSelect Controller
Add to your `SELECT` element an attribute `data-controller="slim-select"`.

## Rails View Helpers
### Iconify SVG image helper
This helper provides access to [Iconify Icon Framework](https://iconify.design).

You must include the `topsail_common/application` ES6 package for this to work!

### Using View Helper body_class
In view (probably `views/layouts/application.html.erb`) render html *body* element like
```html
        <body class="<%= body_class %>">
```
that way you can scope your CSS selectors to specific controllers, actions and variants

### Using View Helper _menu_links(menu_configs_ary)_
Generates a menu based on the _menu_configs_ary_ and actual Pundit permissions.

Example:
```ruby
menu_links [
    {model: UserProfile, icon: "face"},
	{label: "More", submenu: [
		{model: Company, permitted: true}
	]}
]
```
Make sure you include ```@import "topsail_common";``` in your scss stylesheet asset (see [scss variables](#markdown-header-using-scss-variables-functions-and-styles) for customization via SCSS variables).

Valid keys in the config element hashes are:

* label (defaults to I18n of _model_)
* icon
* model
* url (defaults to index path of _model_)
* permitted (defaults to ```policy(model).show_in_menu?```; for submenus *true* if any nested link permitted)
* submenu (an array of more menu configs)
* html_opts (extra html options for the menu link or submenu UL tag)

### Using Form Builder Helper _top_label_

* follow the instructions for installing the _ts-tooltip_ web component
* if you need help text for a top_label, add it in the i18n file with key **en:help:the_model_name:attribute_html** or **en:help:the_model_name:attribute**

```ruby
form.top_label(attribute_name) do
    form.text_field(attribute_name) ... etc
end
```

## Using Scopes
### scopes_for_boolean
