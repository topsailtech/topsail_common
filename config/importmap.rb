pin_all_from File.expand_path('../app/assets/javascripts/topsail_common', __dir__), under: 'topsail_common'

# Looks like I can not vendorize javascript in engines. Pinning to CDN...
pin '@iconify/iconify', to: 'https://ga.jspm.io/npm:@iconify/iconify@2.1.2/dist/iconify.min.js'
# Can't just point to "https://unpkg.com/@topsail/ts-tooltip" becuase Safari blows up w/ CORS error
pin 'ts-tooltip', to: 'https://unpkg.com/@topsail/ts-tooltip@0.0.6/dist/ts-tooltip.js'

# make sure to add `@import url("")` for each library's CSS to the stylesheets!
pin 'tom-select', to: 'https://ga.jspm.io/npm:tom-select@2.3.1/dist/js/tom-select.complete.js'
pin 'dragula', to: 'https://ga.jspm.io/npm:dragula@3.7.3/dist/dragula.js'
pin 'toastify-js', to: 'https://ga.jspm.io/npm:toastify-js@1.12.0/src/toastify.js' # css in growl.scss
